# Zoneminder S3 Sync

## Requirements
- Node `10.x`

## Build
1. Run `npm install` on the cloned git repo
2. Run `npm run build`
3. Output file is `s3-sync.tar.gz`

## Install

### Setup AWS credentials
- Create an IAM user with API access with the permissions as defined below updating `{{ bucket-name }}` as required:
```
{
    "Version": "2012-10-17",
    "Statement": [
        {
            "Sid": "VisualEditor0",
            "Effect": "Allow",
            "Action": "s3:*",
            "Resource": "arn:aws:s3:::{{ bucket-name }}/*"
        }
    ]
}
```
- Ensure you have your AWS credentials for this user under `~/.aws/credentials` on the host you wish to run it on:
```
[default]
aws_access_key_id = YOUR_ACCESS_KEY_ID
aws_secret_access_key = YOUR_SECRET_ACCESS_KEY
```

### Install app
1. Copy and extract `s3-sync.tar.gz` to location you wish (suggested: `/opt/s3-sync`)
2. Enter the extracted directory
3. Update `config/production.yml` as required for your host
3. Run `npm install --only=prod`
4. Install `pm2` globally if it doesn't already exist with `npm install pm2 -g` 
5. Run `pm2 start process.yml`

### Start on boot
- Run `pm2 startup` and `pm2 save`