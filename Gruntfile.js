module.exports = function(grunt) {
  grunt.initConfig({
    pkg: grunt.file.readJSON("package.json"),
    clean: ["dist/*"],
    ts: {
      default: {
        tsconfig: "./tsconfig.json"
      }
    },
    copy: {
      main: {
        files: [
          {
            expand: true,
            src: "package.json",
            dest: "dist/"
          },
          {
            expand: true,
            src: "package-lock.json",
            dest: "dist/"
          },
          {
            expand: true,
            src: "process.yml",
            dest: "dist/"
          },
          {
            expand: true,
            src: "config/production.yml",
            dest: "dist/"
          }
        ]
      }
    },
    shell: {
      default: {
        command: ["cd dist", "tar czf ../s3-sync.tar.gz *"].join(" && ")
      }
    }
  });

  grunt.loadNpmTasks("grunt-contrib-clean");
  grunt.loadNpmTasks("grunt-ts");
  grunt.loadNpmTasks("grunt-contrib-copy");
  grunt.loadNpmTasks("grunt-shell");

  grunt.registerTask("default", ["clean", "ts", "copy", "shell"]);
};
