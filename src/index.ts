import { relative } from "path";

import * as config from "config";
import watch from "node-watch";
import { S3 } from "aws-sdk";
import { createReadStream, statSync } from "fs-extra";
import * as moment from "moment";
const ms = require("ms");
const throttleProxy = require("throttle-proxy");
// tslint:disable-next-line:variable-name
const SocksProxyAgent = require("socks-proxy-agent");

import buildLogger from "./logger";

const log = buildLogger("index.js");

const ZM_EVENTS_PATH: string = config.get("zoneminder.events_path");
const ZM_EVENTS_FILTER: string = config.get("zoneminder.events_filter");

const AWS_S3_BUCKET_NAME: string = config.get("aws.s3_bucket_name");
const AWS_S3_FILE_EXPIRES = config.get("aws.s3_file_expires");
const AWS_S3_THROTTLE_MBPS: number = config.get("aws.s3_throttle_mbps"); // Megabits per second

// Create our internal proxy to rate limit our requests
throttleProxy({
  port: 1080,
  incomingSpeed: (AWS_S3_THROTTLE_MBPS * 1000 * 1000) / 8,
  outgoingSpeed: (AWS_S3_THROTTLE_MBPS * 1000 * 1000) / 8,
  delay: 0,
});

const httpsAgent = new SocksProxyAgent({
  host: "localhost",
  port: 1080,
  protocol: "socks5",
});

const s3 = new S3({
  apiVersion: "2006-03-01",
  httpOptions: { agent: httpsAgent },
});

log.info(`Starting recursive watch for directory: ${ZM_EVENTS_PATH}`);

watch(ZM_EVENTS_PATH, { recursive: true }, (eventType, filename) => {
  const relativePath = relative(ZM_EVENTS_PATH, filename);

  log.info(`'${relativePath}' - Change type '${eventType}' detected`);

  if (!relativePath.match(new RegExp(ZM_EVENTS_FILTER, "g"))) {
    log.info(`'${relativePath}' - Skipping (didn't match filter)`);
    return;
  }

  // Update/create
  if (eventType === "update") {
    if (!statSync(filename).isFile()) {
      log.info(`'${relativePath}' - Skipping (not a file)`);
      return;
    }

    log.info(`'${relativePath}' - Uploading file`);

    const fileStream = createReadStream(filename);
    fileStream.on("error", (error) => {
      log.error(`'${relativePath}' File read error:`);
      log.error(error);
    });

    s3.upload(
      {
        Bucket: AWS_S3_BUCKET_NAME,
        Key: relativePath.replace("\\", "/"),
        Body: fileStream,
        Expires: moment().add(ms(AWS_S3_FILE_EXPIRES), "milliseconds").toDate(),
      },
      (error, data) => {
        if (error) {
          log.error(`'${relativePath}' - S3 upload error`);
          log.error(error);
        } else {
          log.info(`'${relativePath}' - Uploaded file to s3`);
        }
      }
    );
  } else {
    // Delete (skip)
  }
});
