import * as path from "path";
import * as config from "config";

import { createLogger, format, transports, Logger } from "winston";

// Setup logger
const LOG_FILENAME: string | undefined = config.has("logging.filename")
  ? config.get("logging.filename")
  : undefined;
const LOG_LEVEL: string = config.has("logging.level")
  ? config.get("logging.level")
  : "info";

const myFormat = format.printf(
  ({ level, message, correlationId, filename, timestamp }) => {
    const displayCI = correlationId ? ` - [${correlationId}]` : "";
    return `${timestamp} ${level}: ${filename}${displayCI}: ${message}`;
  }
);

const logger = createLogger({
  level: LOG_LEVEL,
  transports: [
    new transports.File({
      filename: path.resolve("./" + LOG_FILENAME)
    }),
    new transports.Console({
      format: format.combine(format.colorize(), format.timestamp(), myFormat)
    })
  ],
  format: format.combine(format.timestamp(), myFormat)
});

export default function buildLogger(
  moduleFilename: NodeModule | string
): Logger {
  const filename =
    typeof moduleFilename === "string"
      ? moduleFilename
      : moduleFilename.id.substring(
          moduleFilename.id.lastIndexOf(path.sep) + 1
        );
  return logger.child({ filename });
}
